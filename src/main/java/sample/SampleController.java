package sample;

import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SampleController implements Initializable {
    ObservableList<String> sourceStationList = FXCollections.observableArrayList();
    ObservableList<String> destinationStationList = FXCollections.observableArrayList();

    private SeatSelection seatSelection;
    @FXML
    public ChoiceBox<String> sourceStation;
    public ChoiceBox<String> destinationStation;

    @FXML
    Button getSeatSelection = new Button();

    @FXML
    DatePicker datePicker = new DatePicker();
    @FXML
    public Label dateOfJourney;
    @FXML
    public Label dateOfJourneyTicket;

    @FXML
    public Label sourceStationTicketLabel;

    @FXML
    public Label destinationStationTicketLabel;

    EventHandler<ActionEvent> eventReservation = new EventHandler<ActionEvent>() {
        public void handle(ActionEvent e) {
            // get the date picker value
            LocalDate i = datePicker.getValue();
            // get the selected date
            dateOfJourney.setText("Date : " + i);
            dateOfJourneyTicket.setText("Date : " + i);
        }
    };



        @Override
        public void initialize(URL url, ResourceBundle resourceBundle) {
            loadSourceStation();
            loadDestinationStation();
            datePicker.setShowWeekNumbers(true);
            datePicker.setOnAction(eventReservation);
            seatSelection = new SeatSelection();
            getSeatSelection.setOnMouseClicked((event) -> {
                try {
                    FXMLLoader loader = new FXMLLoader(new File("src/main/java/sample/seatSelection.fxml").toURI().toURL());
                    Scene scene = new Scene(loader.load(), 400, 500);
                    Stage stage = new Stage();
                    stage.setTitle("Seat Selection Window");
                    stage.setScene(scene);
                    stage.show();
                } catch (IOException e) {
                    Logger logger = Logger.getLogger(getClass().getName());
                    logger.log(Level.SEVERE, "Failed to create new Window.", e);
                }
            });
        }

        private void loadSourceStation() {
            sourceStationList.removeAll(sourceStationList);
            String braila = "Braila";
            String galati = "Galati";
            String bucuresti = "Bucuresti";
            sourceStationList.addAll(braila, galati, bucuresti);
            sourceStation.getItems().addAll(sourceStationList);
            sourceStation.getSelectionModel().selectedIndexProperty().addListener((ObservableValue<? extends Number> ov,
                                                                                   Number old_val, Number new_val) -> {
                String[] stations = new String[]{"Braila", "Galati", "Bucuresti"};
                sourceStationTicketLabel.setText("Source Station : " + stations[new_val.intValue()]);
            });
        }

        private void loadDestinationStation() {
            destinationStationList.removeAll(destinationStationList);
            String braila = "Braila";
            String galati = "Galati";
            String bucuresti = "Bucuresti";
            destinationStationList.addAll(braila, galati, bucuresti);
            destinationStation.getItems().addAll(destinationStationList);
            destinationStation.getSelectionModel().selectedIndexProperty().addListener((ObservableValue<? extends Number> ov,
                                                                                        Number old_val, Number new_val) -> {
                String[] stations = new String[]{"Braila", "Galati", "Bucuresti"};
                destinationStationTicketLabel.setText("Destination Station : " + stations[new_val.intValue()]);
            });
        }


    }
